import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SchoolManagement {
	public static void main(String args[])
	{	
		final List<Institution> institutions = new ArrayList<>();
		
		initializeFiveInstitutions(institutions);

		final List<Student> students = new ArrayList<>();
		/*
		 * creating 50 Students and calculate AverageGrade for the institutions
		 */
		double sumGrade;
		for (int i = 0; i < 5; i++) 
		{
			sumGrade = 0;
			for(int j = 0; j < 10; j++) 
			{
			final String name = "Student" + (i*10 + j + 1);
			final int age = i*10 + j + 10;
			String gender = "male";
			if (j % 2 != 0) 
			{
				gender = "female";	
		    }
			
			Random r = new Random();
			double grade = 2 + (6 - 2) * r.nextDouble();
			DecimalFormat df = new DecimalFormat("#.##");
			grade = Double.parseDouble(df.format(grade));
			
			sumGrade += grade;
			
			final int entityId = i + 1;
			
			students.add(new Student(name, age, gender, grade, entityId));
			
			final String insitutionName = institutions.get(i).getName();
			
			System.out.println("Hello " + students.get(i*10 + j).getName() + " and welcome to " + insitutionName);
			// for clarity and testing purposes uncomment next row
			// System.out.println("with grade : " + students.get(i*10 + j).getGrade() + " and gender : " + students.get(i*10 + j).getGender());
			}
			institutions.get(i).setAverageGrade(sumGrade/10);
			System.out.println();
		}
	
		double topMaleGrade = 0;
		String topMaleStudentName = "";
		double topFemaleGrade = 0;
		String topFemaleStudentName = "";
		double totalIncome = 0;
		double topContribution = 0;
		String topContributorName = "";
		
		for (int i = 0; i < 5; i++) 
		{
			for(int j = 0; j < 10; j++) 
			{
				final Student currentStudent = students.get(i*10 + j);
				final double currentGrade = currentStudent.getGrade();
				final String currentStudentName = currentStudent.getName();
				
				if(j%2 != 0) 
				{
					if(currentGrade > topFemaleGrade)
					{
						topFemaleGrade = currentGrade;
						topFemaleStudentName = currentStudentName;
					}
				}
				else 
				{
					if(currentGrade > topMaleGrade)
					{
						topMaleGrade = currentGrade;
						topMaleStudentName = currentStudentName;
					}
				}
				
				final double currentContribution = currentStudent
						.calculatePayment(institutions.get(i).getAverageGrade(), institutions.get(i).getTax());
				
				totalIncome += 	currentContribution;
				
				if(currentContribution > topContribution)
				{
					topContributorName = currentStudentName;
				}
			}
					
			System.out.printf("Avarage grade from all students in %s is: %.2f %n", institutions.get(i).getName() , institutions.get(i).getAverageGrade());
		}
		
		System.out.println();
		
		if(topMaleGrade > topFemaleGrade)
		{
			System.out.printf("The top performing Student is %s with grade: %.2f %n", topMaleStudentName, topMaleGrade);
		}
		else 
		{
			System.out.printf("The top performing Student is %s with grade: %.2f %n", topFemaleStudentName, topFemaleGrade);
		}
		
		System.out.printf("The top performing female Student is %s with grade: %.2f %n", topFemaleStudentName, topFemaleGrade);
		System.out.printf("The top performing male Student is %s with grade: %.2f %n", topMaleStudentName, topMaleGrade);
		System.out.printf("The total income from all schools and unis are : %.2f %n", totalIncome);
		System.out.printf("The top contributor's name is: %s", topContributorName);	
	}

	private static void initializeFiveInstitutions(final List<Institution> institutions) {
		institutions.add(new School(1, "MG Petar Beron", "Chaika"));
		institutions.add(new School(2, "Dimcho Debelqnov", "Center"));
		institutions.add(new School(3, "4th high school", "Briz"));
		institutions.add(new University(4, "TU Varna", "Studenska str 1"));
		institutions.add(new University(5, "Free Varna Uni", "Golden Sands"));
	}
}
