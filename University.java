
public class University extends Institution{
	
	private final int TAX = 50;

	public University(int id, String name, String address) 
	{
		super(id, name, address);
	}
	
	@Override
	public int getTax()
	{
		return this.TAX;
	}
}
