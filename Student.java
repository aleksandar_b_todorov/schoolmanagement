
public class Student {
	private String name;
	private int age;
	private String gender;
	private double grade; //random between 2 and 6 and 2 decimals
	private int entityId;
	
	public Student(String name, int age,String gender, double grade, int entityId)
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.grade = grade;
		this.entityId = entityId;
	}
	

	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getAge() {
		return age;
	}



	public void setAge(int age) {
		this.age = age;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public double getGrade() {
		return grade;
	}



	public void setGrade(double grade) {
		this.grade = grade;
	}



	public int getEntityId() {
		return entityId;
	}



	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}



	public double calculatePayment(double avgGrade, int tax)
	{
		return this.age/avgGrade * 100 + tax;
	}
}
